
ARG BASE=python:3.7-slim

FROM ${BASE}

ARG FLASK_APP=app.py
ARG APP_AES_KEY=somekey234876283
ENV FLASK_APP=${FLASK_APP}
ENV APP_AES_KEY=${APP_AES_KEY}

LABEL Description "Testing application from Devops test"
LABEL Maintainer "Luis Miguel Sáez Martín"

RUN mkdir /app
WORKDIR /app

ADD app.py .
ADD requirements.txt .

RUN apt-get update -y \
    && apt-get install -y gcc

RUN pip install -r requirements.txt

RUN useradd -d /app -s /usr/sbin/nologin -u 1000 tiqets \
    && chown -R tiqets. /app

USER tiqets

ENTRYPOINT [ "flask", "run", "--host=0.0.0.0" ]
