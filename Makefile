
build:
	docker build -t tiqets:test .

run:
	docker run -d --name test -p 8080:5000 -e FLASK_ENV=development tiqets:test

test:
	echo 'test' | curl -XPOST http://localhost:8080/encrypt

clean:
	docker rm -f test
