# Devops Test 01

## Containerize an application

This repository contains a very simple application written in Python that encrypts and decrypts secrets.

### Instructions for running the app

```
> pip install -r requirements.txt

> FLASK_APP=app.py APP_AES_KEY=somekey234876283 flask run
```

Once it's running you can test the endpoints with

```
> echo 'hello' | curl -X POST http://localhost:5000/encrypt
```

Your goal is to improve this application to be able to run in kubernetes.

## Deliverables

- New files and modifications to the application to be able to run in Minikube (Linux) or Docker for Mac (macOS)
- Instructions on how to run the application
- BONUS: configure GitLab CI

## Notes

- Don't spend more than 2 hours on this assignment.
- Think about logging and monitoring. Suggest and implement improvement.
- Security is also our priority. What should be improved or changed? If you can, implement the changes.
- You should explain, why you made a specific technical decision.


## Resolution notes

- First, I'm defining the docker image build in `Dockerfile`

  - I selected the python 3.9 base image ( slim flavor to ensure it's lighter )
  - To ensure a higher security level, I've created a non-root user to execute the application
  - To be able of compile python requirements, I installed the `gcc` package
  - I'm not using a multi-stage build, because it's dificult to implement for python builds and the image isn't too heavy
  - Added `--host` option to be able to connect to the running process from outside the container
  - Added `BASE` argument, to be able to modify the base image on build time

- The application is returning the following exception while processing a request on `/encrypt` endpoint:
  ```
  ...
    File "/usr/local/lib/python3.9/site-packages/Crypto/Random/_UserFriendlyRNG.py", line 129, in read
      self._ec.collect()
    File "/usr/local/lib/python3.9/site-packages/Crypto/Random/_UserFriendlyRNG.py", line 77, in collect
      t = time.clock()
  AttributeError: module 'time' has no attribute 'clock'
  ```
  It seems that `time.clock` is deprecated for python versions above 3.7, so I changed the initial 3.9 version to 3.7. We could use another crypto library too, but it's more difficult to implement ( maybe to be done later )

- Added `stdout` logging handler to be able to collect logs on containerized environments

- For monitoring purposes, metrics could be exported to be scrapped by prometheus, for example, with `prometheus-flask-exporter` library

- Added `prometheus-flask-exporter==0.18.2` dependency and basic configuration into the application code

- Added `Makefile` for easier local testing and documented commands

- Added Kubernetes manifests in `k8s` directory. All resources in the same file make it simple

- Added a very simple Gitlab pipeline definition, just to build the container and test that it's functional before uploading to registry

  - Configured docker login username and password as Gitlab managed variables
  - Image versioning is fixed as 0.0.1 for testing purposes. For real life pipeline, we could use python `pbr` manage versioning, for example, from git tags.
  - All the steps ( scripts ) are included into the same stage, but they could be splitted into several independent ones

## How to run the application

### Build
```
make build
```

### Run
```
make run
```

### Test
```
make test
```

### Clean
```
make clean
```

## How to run the application ( Kubernetes )

### Build image
```
docker build -t luismiguelsaez/encrypter:0.0.1 .
```

### Docker login into personal account
```
docker login -u luismiguelsaez
```

### Push image
```
docker push luismiguelsaez/encrypter:0.0.1
```

### Deploy to minikube
```
kubectl apply -f k8s
```

### Test ( through tunnel against k8s service )
```
kubectl port-forward svc/encrypter 8080:80
curl -XPOST localhost:8080/encrypt -d'hello'
```
